// Create express app
var express = require("express")
var app = express()
var db_miRNA_tissues = require("./database_miRNA_tissues.js")
var db_miRNA_biofluids = require("./database_miRNA_biofluids.js")
var db_miRNA_targets = require("./database_miRNA_targets.js")

// Server port
var HTTP_PORT = 8080 

// Start server
app.listen(HTTP_PORT, () => {
    console.log("Server running on port %PORT%".replace("%PORT%",HTTP_PORT))
});
// Root endpoint
app.get("/", (req, res, next) => {
    res.json({"message":"Ok"})
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  //res.header("Access-Control-Allow-Origin", "https://microrna-atlas.omics.kitchen");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

/*
 * API endpoint to return all miRNA IDs in the database
 */ 
app.get("/miRNAs", (req, res, next) => {
    var sql = "SELECT DISTINCT miRNA_id "+
	      "FROM miRNA_tissueAtlas_quants "+
	      "ORDER BY miRNA_id"
    var params = []
    db_miRNA_tissues.all(sql, params, (err, result) => {
        if (err) {
	  res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            result
        })
      });
});

/*
 * API endpoint to return all tissue expression data for a given miRNA ID
 */
app.get("/miRNA/tissue/:id", (req, res, next) => {
    var sql = "SELECT miRNA_tissueAtlas_quants.miRNA_id, miRNA_tissueAtlas_quants.exprs_RPM, "+
	      "       miRNA_tissueAtlas_quants.exprs_DESeq, miRNA_tissueAtlas_quants.exprs_percentile, "+
              "       miRNA_tissueAtlas_sampleInfo.subject_id, miRNA_tissueAtlas_sampleInfo.tissue"+
              " FROM miRNA_tissueAtlas_quants "+
              " INNER JOIN miRNA_tissueAtlas_sampleInfo "+
              " ON miRNA_tissueAtlas_sampleInfo.sample_id = miRNA_tissueAtlas_quants.sample_id"+ 
              ' WHERE miRNA_id IN ( ? ) '+
              " ORDER BY miRNA_tissueAtlas_quants.miRNA_id,miRNA_tissueAtlas_sampleInfo.tissue"
    //var params = [req.params.id];//.replace(",",'","'));
    var params = [req.params.id.replace(",",'","')]
    db_miRNA_tissues.all(sql, params, (err, result) => {
        if (err) {
          //res.status(400).json({"error":err.message, "params":params, "cmd":sql});
          return;
        }
        res.json({
            //"message":"success",
            //"call":sql,
	    //"params":params,
	    //"newParams":params[0].replace(",",'","'),
	    result
        })
      });
});



/*
 * API endpoint to return all biofluid expression data for a given miRNA ID
 */
app.get("/miRNA/biofluid/:id", (req, res, next) => {
    var sql = "SELECT miRNA_biofluidAtlas_quants.miRNA_id, miRNA_biofluidAtlas_quants.exprs_RPM, "+
              "       miRNA_biofluidAtlas_quants.exprs_DESeq, miRNA_biofluidAtlas_quants.exprs_percentile, "+
              "       miRNA_biofluidAtlas_sampleInfo.biofluid"+
              " FROM miRNA_biofluidAtlas_quants "+
              " INNER JOIN miRNA_biofluidAtlas_sampleInfo "+
              " ON miRNA_biofluidAtlas_sampleInfo.sample_id = miRNA_biofluidAtlas_quants.sample_id"+
              ' WHERE miRNA_id IN ( ? ) '+
              " ORDER BY miRNA_biofluidAtlas_quants.miRNA_id,miRNA_biofluidAtlas_sampleInfo.biofluid"
    //var params = [req.params.id];//.replace(",",'","'));
    var params = [req.params.id.replace(",",'","')]
    db_miRNA_biofluids.all(sql, params, (err, result) => {
        if (err) {
          //res.status(400).json({"error":err.message, "params":params, "cmd":sql});
          return;
        }
        res.json({
            //"message":"success",
            //"call":sql,
            //"params":params,
            //"newParams":params[0].replace(",",'","'),
            result
        })
      });
});



/*
 * API endpoint to return the miRNA family for a given miRNA ID
 */
app.get("/miRNA_family/:id", (req, res, next) => {
    var sql = "SELECT DISTINCT * "+
              " FROM targetScan_miRNA_families "+
              ' WHERE miRNA_id IN ( ? ) '
    var params = [req.params.id.replace(",",'","')]
    db_miRNA_targets.all(sql, params, (err, result) => {
        if (err) {
          //res.status(400).json({"error":err.message, "params":params, "cmd":sql});
          return;
        }
        res.json({
            result
        })
      });
});


/*
 * API endpoint to return all miRNA/mRNA targets for a given miRNA ID
 */
app.get("/miRNA_targets/:id", (req, res, next) => {
    var sql = "SELECT DISTINCT targetScan_targets.Gene_Symbol, "+
              "                targetScan_targets.Gene_ID_simple, "+
	      "                targetScan_targets.Gene_ID "+
              " FROM targetScan_targets "+
              " INNER JOIN targetScan_miRNA_families "+
              " ON targetScan_miRNA_families.miR_family = targetScan_targets.miR_Family "+ 
              ' WHERE targetScan_miRNA_families.miRNA_id IN ( ? ) '+
              " ORDER by targetScan_targets.Gene_Symbol"
    var params = [req.params.id.replace(",",'","')]
    db_miRNA_targets.all(sql, params, (err, result) => {
        if (err) {
          //res.status(400).json({"error":err.message, "params":params, "cmd":sql});
          return;
        }
        res.json({
            result
        })
      });
});


// Default response for any other request
app.use(function(req, res){
    res.status(404);
});


