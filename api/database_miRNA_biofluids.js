var sqlite3 = require('sqlite3').verbose()
var md5 = require('md5')

const DBSOURCE = "DB_miRNA_biofluidAtlas.sqlite"

let db_miRNA_biofluids = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
      // Cannot open database
      console.error(err.message)
      throw err
    }else{
        console.log('Connected to: '+DBSOURCE)
    }
});


module.exports = db_miRNA_biofluids

