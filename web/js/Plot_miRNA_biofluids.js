/*
 * links to the DOM
 */
var htmlElementName_miRNA_biofluid = 'div_plot_miRNA_biofluids';
var htmlElement_miRNA_biofluid = document.getElementById(htmlElementName_miRNA_biofluid);

/*
 * Plotly layout
 */
var layout_miRNA_biofluid = {
  width: 280,
  height: 510,
  //height: 465,
  xaxis: {
    tickangle: 45,
    categoryorder: "trace"
  },
  yaxis: {
    title: 'RNA abundance (normalised RPM)',
    zeroline: false,
    type: 'linear',
    autorange: true
  },
  margin: {
    t: 5,
    b: 100,
    l: 50,
    r: 40
  },
  updatemenus: [
  {
    direction: "right",
    xanchor: 'left',
    yanchor: "top",
    //pad = list('r'= 0, 't'= 10, 'b' = 10),
    x: -0.06,
    ////y: 1.27,
    //y: 1.10,
    y: -0.25,
    buttons: [{
      method: "relayout",
      args: ["yaxis.type", "linear"],
      label: "linear"
    },
    {
      method: "relayout",
      args: ["yaxis.type", "log"],
      label: "log"
    }]
  }],
  //boxmode: 'group',
  showlegend: false,
  legend: {
    "orientation": "h",
    xanchor: "center",
    bgcolor: 'rgba(0, 0, 0, 0.0)',
    y: 1.15,
    x: 0.5
  }
};





var cfData_miRNA_biofluid, cfDim_miRNA_biofluid;
var plotOrdering_miRNA_biofluid = "median";

function instanciatePlot_miRNA_biofluids(miRNA_id) {
//console.log("http://data.omics.kitchen/miRNAatlasAPI/miRNA/biofluid/"+miRNA_id);
  d3.json("http://data.omics.kitchen/miRNAatlasAPI/miRNA/biofluid/"+miRNA_id).then(function(data_in){
    var data = data_in.result;
    //console.log(data);

    data.forEach(function(x) {
      x.exprs_RPM = (+x.exprs_RPM)+0.01;
      x.exprs_DESeq = (+x.exprs_DESeq)+0.01;
      x.exprs_percentile = +x.exprs_percentile;
    });

    cfData_miRNA_biofluid = crossfilter(data)
    cfDim_miRNA_biofluid = cfData_miRNA_biofluid.dimension(function(d) { return d.biofluid; });
    plotAbundance_miRNA_biofluid();
  })
}

function plotAbundance_miRNA_biofluid(){
  var toPlot = [];
  var tmp_dim = cfData_miRNA_biofluid.dimension(function(d) { return d.biofluid; });
  var tmp_group = tmp_dim.group(function(d) { return d; });
  var categories = nonZeroValues(tmp_group.all());
  tmp_group.dispose();
  //console.log(categories);

  var tmp_dat = tmp_dim.top(Infinity);
  //console.log(tmp_dat);

  //alert(unpack(tmp_dat, "tissue"));
  // Re-order?
  //console.log("ordering: "+plotOrdering_miRNAatlas);
  if(plotOrdering_miRNA_biofluid === "median"  ||  plotOrdering_miRNA_biofluid === "mean"){
    // calculate median and mean values

    // get the tissues - these are what we'll be ordering on
    var tmp_dim_tissues = cfData_miRNA_biofluid.dimension(function(d) { return d.biofluid; });
    var tmp_group2 = tmp_dim_tissues.group(function(d) { return d; });
    var tissues = nonZeroValues(tmp_group2.all());
    tmp_group2.dispose();

    // calculate the mean and median of each tissue
    var items = [];
    for(var i = 0; i < tissues.length; i++){
      var thisTissue = tissues[i];
      tmp_dim_tissues.filter(function(d){ return d === thisTissue });
      var tmp_dat = tmp_dim_tissues.top(Infinity);
      //console.log(tmp_dat);

      items.push({category:thisTissue, median:median(unpack(tmp_dat, 'exprs_RPM')), mean:mean(unpack(tmp_dat, 'exprs_RPM'))});
      tmp_dim_tissues.filter();
    }
    //console.log(items);

    // order categories by mean or median (decreasing)
    if(plotOrdering_miRNA_biofluid === "median"){
      items.sort(function (a, b) {
        return b.median - a.median;
      });
    }
    else{
      items.sort(function (a, b) {
        return b.mean - a.mean;
      });
    }
    //console.log(items);
    //console.log(unpack(items,"category"));

    // pass this ordering to the layout
    layout_miRNA_biofluid.xaxis.categoryorder = "categoryarray";
    layout_miRNA_biofluid.xaxis.categoryarray =  unpack(items,"category");
  }

	/*
    var tmp_dat = tmp_dim.top(Infinity);
    //console.log(tmp_dat);
    var toPlot = { x:unpack(tmp_dat, "biofluid"),
                   y:unpack(tmp_dat, 'exprs_RPM'),
                   name: "",
                   type:'box',
                   hoverinfo: 'skip'
                   //mode: 'markers'
                 };
*/
  // Create the plot data.  We do not need to specifiy the order of x-axis values here as this is handled by the 'categoryarray' passed to the layout
  var toPlot = [];
  //console.log(tissues);
  for(var i = 0; i < tissues.length; i++){
    var thisCategory = tissues[i];
    tmp_dim_tissues.filter(function(d){ return d === thisCategory });
    var tmp_dat = tmp_dim_tissues.top(Infinity);
    //console.log(tmp_dat);
    toPlot[i] = { x:unpack(tmp_dat, 'biofluid'),
                  y:unpack(tmp_dat, 'exprs_RPM'),
                  name: thisCategory,
                  type: 'box'
                };
    tmp_dim_tissues.filter();
  }

  //console.log(toPlot);
  //var toPlot = {
  //  x: unpack(tmp_dat, ""),
  //  type: 'histogram',
  //};

  //var data = [toPlot];
  //console.log(data);
  Plotly.newPlot(htmlElementName_miRNA_biofluid, toPlot, layout_miRNA_biofluid, {displayModeBar: false});

}


/*
 * Function to toggle plotting by tissue or by sex
 */
function updatePlot_miRNA_biofluids() {
  // get references to select list and display text box
/*  var sel = document.getElementById('select_GTEx_plotBy');
  var opt = sel.options[sel.selectedIndex];
  if(opt.value == "GTEx_byTissue"){ currentPlot_GTEx = "tissue"; }
  else if(opt.value == "GTEx_bySex"){ currentPlot_GTEx = "sex"; }
  else if(opt.value == "GTEx_bySystem"){ currentPlot_GTEx = "system"; }
  */


  var ele = document.getElementsByName("sortOption_miRNA_biofluids");
  for(i = 0; i < ele.length; i++) {
    if(ele[i].checked)
      plotOrdering_miRNA_biofluid = ele[i].value;
  }


//        sel = document.getElementById('select_GTEx_sortBy');
//  opt = sel.options[sel.selectedIndex];
//  if(opt.value == "GTEx_sort_alphabetical"){ plotOrdering_miRNAatlas = "alphabetical"; }
//  else if(opt.value == "GTEx_sort_median"){ plotOrdering_miRNAatlas = "median"; }
//  else if(opt.value == "GTEx_sort_mean"){ plotOrdering_miRNAatlas = "mean"; }

  Plotly.purge(htmlElementName_miRNA_biofluid);
  plotAbundance_miRNA_biofluid();
};
