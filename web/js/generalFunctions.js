




/*
 * Search autocomplete
 */
function populate_miRNAList(){
  d3.json("http://data.omics.kitchen/miRNAatlasAPI/miRNAs").then(function(data_in){
    availableTags = unpack(data_in.result,"miRNA_id");
    //console.log(availableTags);
    var str = '';
    for (var i=0; i < availableTags.length;++i){
      str += '<option value="'+availableTags[i]+'" />'; // Storing options in variable
    }
    var miRNA_list=document.getElementById("miRNAList");
    miRNA_list.innerHTML = str;
  } );
}






var urinaryTissues_ExoDx = ['Urine'];
var urinaryTissues_GTEx = ['bladder','kidney','testis','prostate'];
var urinaryTissues_TCGA = ['bladder','kidney','testis','prostate'];
var urinaryTissues_HPM = ['urinary bladder','kidney','testis','prostate'];


function now() {
  return (new Date()).getTime()
}


function onlyUnique(value, index, self) { 
	return self.indexOf(value) === index;
}
 
function containsObject(obj, list) {
	var result = false;
	for (var i = 0; i < list.length; i++) {
  	if (list[i] == obj) {
    		result = true;
        break;
  	}
	}
	return result;
}


function getAllIndices(arr, vals) {
    var indexes = [], i;
    for(i = 0; i < arr.length; i++){
    	for(j = 0; j < vals.length; j++){
        	if (arr[i] === vals[j]){
        	    indexes.push(i);
        	    break;
        	}
    	}
    }
    return indexes;
}
function getAllIndices_excluding(arr, vals) {
	var indexes = [], i;
	for(i = 0; i < arr.length; i++){
		if (!vals.includes(arr[i])){
			indexes.push(i);
		}
	}
    return indexes;
}

function subsetArrayByIndices(arrayToSubset, indices) {
    var subsetArray = [], i;
    for(i = 0; i < indices.length; i++){
      subsetArray.push(arrayToSubset[indices[i]]);
    }
    return subsetArray;
}


//Helper
function unpack(rows, key) {
	return rows.map(function(row) { return row[key]; });
}


nonZeroValues = function(arr){
  var tmp_values = unpack(arr,'value');
  var tmp_keys = unpack(arr,'key');
  var keepValues = [], i;
  for(i = 0; i < tmp_values.length; i++)
    if (tmp_values[i] != 0)
      keepValues.push(tmp_keys[i]);
  return keepValues;
}

/*
 * Get all group names from the provided crossfilter dimension
 */
function getGroupNames(cf_dimension){
  var tmp_group = cf_dimension.group(function(d) { return d; });
  var values = unpack(tmp_group.all(), "key");
  tmp_group.dispose();
  return values;
}




/*
 * Function to toggle urinary tissues
 */
var showingAllTissues = true;
toggleUrinaryTissues = function(){
  if(showingAllTissues){
    //biofluid_ExoDx.filter(function(d){ return containsObject(d, urinaryTissues_ExoDx) });
    ExoDx_setUrinary(true);
    tissue_GTEx.filter(function(d){ return containsObject(d, urinaryTissues_GTEx) });
    tissue_TCGA.filter(function(d){ return containsObject(d, urinaryTissues_TCGA) });
    tissue_HPM.filter(function(d){ return containsObject(d, urinaryTissues_HPM) });
  } else{
  	ExoDx_setUrinary(false);
    tissue_GTEx.filter();
    tissue_TCGA.filter();
    tissue_HPM.filter();
  }

  //Plotly.purge(htmlElementName_ExoDx); 
  //Plotly.newPlot(htmlElementName_ExoDx, makeExoDxPlot(currentPlot_ExoDx), getLayout_ExoDx(), {displayModeBar: false});

  Plotly.purge(htmlElementName_GTEx); 
  makeGTExPlot();

  Plotly.purge(htmlElementName_TCGA); 
  makeTCGAPlot();

  Plotly.purge(htmlElementName_HPM); 
  makeHPMPlot();

  showingAllTissues = !showingAllTissues;
}



/*
 * Function to toggle Sex
 */
//var showingSex = "both";s
filter_sex = function(){

  var sel = document.getElementById('select_sex');
  var opt = sel.options[sel.selectedIndex];
  
  if(opt.value === "select_sex_both"){
    ExoDx_filterAll("sex","on");
    sex_GTEx.filter();
    sex_TCGA.filter();
  } 
  else if(opt.value === "select_sex_female"){
    exclusive_select(selectedSexes, sex_ExoDx, "female");
    sex_GTEx.filter(function(d){ return d === "female" });
    sex_TCGA.filter(function(d){ return d === "female" });
  }
  else if(opt.value === "select_sex_male"){
    exclusive_select(selectedSexes, sex_ExoDx, "male");
    sex_GTEx.filter(function(d){ return d === "male" });
    sex_TCGA.filter(function(d){ return d === "male" });
  }
  
  ExoDx_filterBy("sex");

  Plotly.purge(htmlElementName_GTEx); 
  makeGTExPlot();

  Plotly.purge(htmlElementName_TCGA); 
  makeTCGAPlot();
}



/*
 *
 */
function addCheckbox(parentDiv, id, text, onchange){
    var newDiv = document.createElement("div");
    newDiv.className = "left-align";
    
    var label = document.createElement("label");
    label.className = "label_filter";

    var input = document.createElement("input"); 
    input.className = "filled-in";
    input.setAttribute("type", "checkbox");
    input.setAttribute("onchange", onchange);
    input.setAttribute("id", id);
    input.checked = true; 

    var span = document.createElement("span");
    span.innerHTML = text;

    label.appendChild(input);
    label.appendChild(span);
    newDiv.appendChild(label); 
    document.getElementById(parentDiv).appendChild(newDiv);
    input.checked = true; 
}





function getSortedIndices(values){
  var len = values.length;
  var indices = new Array(len);
  for (var i = 0; i < len; ++i) indices[i] = i;
  indices.sort(function (a, b) { return values[a] < values[b] ? -1 : values[a] > values[b] ? 1 : 0; });
  return indices;
}


function median(values) {
  var median = 0, numsLen = values.length;
  values.sort(function(a, b){return a-b});

  if(numsLen % 2 === 0 ){ // is even
      // average of two middle numbers
      median = (values[numsLen / 2 - 1] + values[numsLen / 2]) / 2;
  } else { // is odd
      // middle number only
      median = values[(numsLen - 1) / 2];
  }
  return median;
}

function mean(values){
  var sum = 0;
  for(var i=0;i<values.length;i++)
    sum += values[i];
  return sum / values.length;
}



function getPlotlyColours(){
  var colours = [ {selected:'#1f77b4',unselected:'#D2E3F0'},  // muted blue
                  {selected:'#ff7f0e',unselected:'#ffe5ce'},  // safety orange
                  {selected:'#2ca02c',unselected:'#d4ecd4'},  // cooked asparagus green
                  {selected:'#d62728',unselected:'#f6d3d4'},  // brick red
                  {selected:'#9467bd',unselected:'#e9e0f1'},  // muted purple
                  {selected:'#8c564b',unselected:'#e8dddb'},  // chestnut brown
                  {selected:'#e377c2',unselected:'#f9e3f2'},  // raspberry yogurt pink
                  {selected:'#7f7f7f',unselected:'#e5e5e5'},  // middle gray
                  {selected:'#bcbd22',unselected:'#f1f1d2'},  // curry yellow-green
                  {selected:'#17becf',unselected:'#d0f2f5'}]; // blue-teal
  return colours;
}


