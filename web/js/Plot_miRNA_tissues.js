/*
 * links to the DOM
 */
var htmlElementName_miRNA_tissue = "div_plot_miRNA_tissues";
var htmlElement_miRNA_tissue = document.getElementById(htmlElementName_miRNA_tissue);


/*
 * Plotly layout
 */
var layout = {
  width: 780,
  height: 510,
  //height: 465,
  xaxis: {
    tickangle: 45,
    categoryorder: "trace"
  },
  yaxis: {
    title: 'RNA abundance (normalised RPM)',
    zeroline: false,
    type: 'linear',
    autorange: true
  },
  margin: {
    t: 5,
    b: 100,
    l: 50,
    r: 40
  },
  updatemenus: [
  {
    direction: "right",
    xanchor: 'left',
    yanchor: "top",
    //pad = list('r'= 0, 't'= 10, 'b' = 10),
    x: -0.06,
    ////y: 1.27,
    //y: 1.10,
    y: -0.25,
    buttons: [{
      method: "relayout",
      args: ["yaxis.type", "linear"],
      label: "linear"
    },
    {
      method: "relayout",
      args: ["yaxis.type", "log"],
      label: "log"
    }]
  }],
  //boxmode: 'group',
  showlegend: false,
  legend: {
    "orientation": "h",
    xanchor: "center",
    bgcolor: 'rgba(0, 0, 0, 0.0)',
    y: 1.15,
    x: 0.5
  }
};






var crossfilter_data, tissue;

function instanciatePlot_miRNA_tissues(miRNA_id) {
  d3.json("http://data.omics.kitchen/miRNAatlasAPI/miRNA/tissue/"+miRNA_id).then(function(data_in){
    //console.log(error);
    //console.log(data_in.result);

    crossfilter_data = crossfilter(data_in.result)
    tissue = crossfilter_data.dimension(function(d) { return d.tissue; });
    plotTissueExpression();
  })
}



function plotTissueExpression(){
  var toPlot = [];
  var tmp_dim = crossfilter_data.dimension(function(d) { return d.tissue; }); 
  var tmp_group = tmp_dim.group(function(d) { return d; });
  var categories = nonZeroValues(tmp_group.all());
  tmp_group.dispose();
  //console.log(categories);

  var tmp_dat = tmp_dim.top(Infinity);
  //console.log(tmp_dat);

  //alert(unpack(tmp_dat, "tissue"));


  // Re-order?
  //console.log("ordering: "+plotOrdering_miRNAatlas);
  if(plotOrdering_miRNAatlas === "median"  ||  plotOrdering_miRNAatlas === "mean"){
    // calculate median and mean values

    // get the tissues - these are what we'll be ordering on
    var tmp_dim_tissues = crossfilter_data.dimension(function(d) { return d.tissue; });
    var tmp_group2 = tmp_dim_tissues.group(function(d) { return d; });
    var tissues = nonZeroValues(tmp_group2.all());
    tmp_group2.dispose();

    // calculate the mean and median of each tissue
    var items = [];
    for(var i = 0; i < tissues.length; i++){
      var thisTissue = tissues[i];
      tmp_dim_tissues.filter(function(d){ return d === thisTissue });
      var tmp_dat = tmp_dim_tissues.top(Infinity);
      //console.log(tmp_dat);

      items.push({category:thisTissue, median:median(unpack(tmp_dat, 'exprs_RPM')), mean:mean(unpack(tmp_dat, 'exprs_RPM'))});
      tmp_dim_tissues.filter();
    } 
    //console.log(items);

    // order categories by mean or median (decreasing)
    if(plotOrdering_miRNAatlas === "median"){
      items.sort(function (a, b) {
        return b.median - a.median;
      });
    }
    else{
      items.sort(function (a, b) {
        return b.mean - a.mean;
      });
    }
    //console.log(items);
    //console.log(unpack(items,"category"));

    // pass this ordering to the layout
    layout.xaxis.categoryorder = "categoryarray";
    layout.xaxis.categoryarray =  unpack(items,"category");
  }


// Create the plot data.  We do not need to specifiy the order of x-axis values here as this is handled by the 'categoryarray' passed to the layout
  /*
  var toPlot = [];
  for(var i = 0; i < categories.length; i++){
    var thisCategory = categories[i];
    tmp_dim.filter(function(d){ return d === thisCategory });
    var tmp_dat = tmp_dim.top(Infinity);
    //console.log(tmp_dat);
    toPlot[i] = { y:unpack(tmp_dat, "tissue"), 
                  x:unpack(tmp_dat, 'exprs_RPM'),
                  name: thisCategory,
                  scatter: 'box'
                };
    tmp_dim.filter();
  }*/

    var tmp_dat = tmp_dim.top(Infinity);
    //console.log(tmp_dat);
    var toPlot = { x:unpack(tmp_dat, "tissue"),
                   y:unpack(tmp_dat, 'exprs_RPM'),
                   name: "",
                   type:'scatter',
	    	   hoverinfo: 'skip',
		   mode: 'markers'
                 };


  //var toPlot = {
  //  x: unpack(tmp_dat, ""),
  //  type: 'histogram',
  //};

  var data = [toPlot];
  //console.log(data);
  Plotly.newPlot(htmlElementName_miRNA_tissue, data, layout, {displayModeBar: false});

}




//var plotOrdering_miRNAatlas = "alphabetical";
var plotOrdering_miRNAatlas = "median";


/*
 * Function to toggle plotting by tissue or by sex
 */
function updatePlot_miRNA_tissues() {
  // get references to select list and display text box
/*  var sel = document.getElementById('select_GTEx_plotBy');
  var opt = sel.options[sel.selectedIndex];
  if(opt.value == "GTEx_byTissue"){ currentPlot_GTEx = "tissue"; }
  else if(opt.value == "GTEx_bySex"){ currentPlot_GTEx = "sex"; }
  else if(opt.value == "GTEx_bySystem"){ currentPlot_GTEx = "system"; }
  */


  var ele = document.getElementsByName("sortOption_miRNA_tissues");
  for(i = 0; i < ele.length; i++) { 
    if(ele[i].checked) 
      plotOrdering_miRNAatlas = ele[i].value; 
  }

	
//	  sel = document.getElementById('select_GTEx_sortBy');
//  opt = sel.options[sel.selectedIndex];
//  if(opt.value == "GTEx_sort_alphabetical"){ plotOrdering_miRNAatlas = "alphabetical"; }
//  else if(opt.value == "GTEx_sort_median"){ plotOrdering_miRNAatlas = "median"; }
//  else if(opt.value == "GTEx_sort_mean"){ plotOrdering_miRNAatlas = "mean"; }
  
  Plotly.purge(htmlElementName_miRNA_tissue); 
  plotTissueExpression();
}; 










/*
 * TARGETS
 */
/*
var crossfilter_data_targets;
function plot_miRNA(miRNA_id) {
  //d3.json("http://data.omics.kitchen/miRNAatlasAPI/miRNA/"+miRNA_id, function(error, data_in) {
  d3.json("http://data.omics.kitchen/miRNAatlasAPI/miRNA_targets/"+miRNA_id).then(function(data_in){
    //console.log(error);
    //console.log(data_in.result);

    //alert(data_in.rows);

    crossfilter_data_targets = crossfilter(data_in.result)
    //tissue = crossfilter_data.dimension(function(d) { return d.tissue; });
    updateTargetsTable();
  })
}

function updateTargetsTable() {

}
*/
